<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\history;
use App\Http\Controllers\PageController;
use App\Http\Controllers\Auth;
use App\user;

class UserController extends Controller
{
    public function upload(Request $request,$id){
        $this->validate($request,[
            'upload_file' => 'required',
        ]);
        $history = new history;
        $file = $request->file('upload_file');
        $history->file_name = $file->getClientOriginalName();
        $history->userid = $id;
        $history->optimized_file_path= 'hello';

        //Move Uploaded File
        $destinationPath = 'uploads/'.$history->userid;
        $file->move($destinationPath,$request->file('upload_file'));
        $basefile = explode('.',basename($file))[0].'.stl';
        rename($destinationPath.'/'.basename($file),$destinationPath.'/'.$basefile);
        $history->file_path = $destinationPath.'/'.$basefile;
        $history->save();

        return redirect('user/'.$id)->with('success', 'File Uploaded');
    }

    public function viewHistory($id){
        $histories = history::where('userid',$id)->get();
        return  $histories;
    }

    public function viewAll(){
        $histories = history::all();
        return  $histories;
    }

}
