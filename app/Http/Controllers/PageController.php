<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PageController extends Controller
{
    public function getHome(){
        return view('home');
    }

    public function getPublic(){
        return view('public');
    }

    public function getLogin(){
        return view('login');
    }

    public function getAbout(){
        return view('about');
    }

    public function getRegister(){
        return view('register');
    }

    public function getProfile($id){
        return view('profile')->with('id',$id);
    }

    public function getHistory($id){
        return view('history')->with('id',$id);
    }

    public function getLogout(){
        return view('logout');
    }

    public function getUser(){
        return view('user');
    }

}
