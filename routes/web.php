<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/' , 'PageController@getHome');

Route::get('/public', 'PageController@getPublic');

Route::get('/about','PageController@getAbout');

Route::get('/user/{id}','PageController@getProfile');

Route::get('/user/{id}/history/','PageController@getHistory');

Route::get("/history/view/all",'UserController@viewAll');

Route::post('/user/uploaded/{id}','UserController@upload');

Route::get('/history/view/{id}','UserController@viewHistory');

Route::get('logout', 'LoginController@logout');

Route::get('user','PageController@getUser');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
