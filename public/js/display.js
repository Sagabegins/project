
let loader = new STLLoader();
function display(id,file){
    loader.load( file, function ( geometry ) {
        var geo = geometry.toIndexed();
        plt(id,geo['attributes']['position'],geo['index']['array']);
    });
}

function plt(id,pos,ind){
    var d= [];
    var e=[];
    var f=[];
    var indi=[];
    var indj=[];
    var indk=[];

    pos= pos['array'];

    for(var i=0;i<pos.length-2;i+=3){
        d.push(pos[i]);
        e.push(pos[i+1]);
        f.push(pos[i+2]);
    }

    for(var i=0;i<ind.length;i+=3){
        indi.push(ind[i]);
        indj.push(ind[i+1]);
        indk.push(ind[i+2]);
    }

    var data=[
    {
    opacity:1.0,
    color:'rgb(53, 216, 230)',
    type: 'mesh3d',
    x: d,
    y: e,
    z: f,
    i: indi,
    j: indj,
    k: indk,
    lighting: {ambient:0.9}
    }
    ];

    var layout = {
       scene:{
           xaxis: {
            grid_color: '#00ffff',
            autorange: true,
            showgrid:true,
            autotick: true,
            linecolor: '#00ffff',
            linewidth: 2,
            showticklabels:false,
            title: '',
          },
          yaxis: {
            grid_color: '#00ffff',
            autorange: true,
            showgrid:true,
            linecolor: '#00ffff',
            linewidth: 2,
            gridwidth: 0.1,
            autotick: true,
            showticklabels:false,
            title: '',
          },
          zaxis: {
            autorange: true,
            showgrid: false,
            zeroline: false,
            showline: false,
            autotick: true,
            ticks: '',
            showticklabels: false,
            title:'',
          }
        },
        paper_bgcolor: '#040404',
        height: 410,
        margin: {
            l: 0,
            r: 0,
            b: 0,
            t: 0,
          },
    };
    Plotly.newPlot(id, data,layout);
}

THREE.BufferGeometry.prototype.toIndexed = function() {
let prec = 0;
let list = [];
let vertices = {};

function store(x, y, z, v) {
    const id = Math.floor( x * prec ) + '_' + Math.floor( y * prec ) + '_'  + Math.floor( z * prec );

    if ( vertices[id] === undefined ) {

        vertices[id] = list.length;


        list.push(v);

    }

    return vertices[id];
}

function indexBufferGeometry(src, dst) {
const position = src.attributes.position.array;
const faceCount = ( position.length / 3 ) / 3;
const type = faceCount * 3 > 65536 ? Uint32Array : Uint16Array;
const indexArray = new type(faceCount * 3);

for ( let i = 0, l = faceCount; i < l; i ++ ) {
    const offset = i * 9;

    indexArray[i * 3    ] = store(position[offset], position[offset + 1], position[offset + 2], i * 3);
    indexArray[i * 3 + 1] = store(position[offset + 3], position[offset + 4], position[offset + 5], i * 3 + 1);
    indexArray[i * 3 + 2] = store(position[offset + 6], position[offset + 7], position[offset + 8], i * 3 + 2);
}
dst.index = new THREE.BufferAttribute(indexArray, 1);

const count = list.length;

for ( let key in src.attributes ) {
    const src_attribute = src.attributes[key];
    const dst_attribute = new THREE.BufferAttribute(new src_attribute.array.constructor(count * src_attribute.itemSize), src_attribute.itemSize);
    const dst_array = dst_attribute.array;
    const src_array = src_attribute.array;

    switch ( src_attribute.itemSize ) {
        case 1:

            for ( let i = 0, l = list.length; i < l; i ++ ) {
                dst_array[i] = src_array[list[i]];
            }

            break;
        case 2:

            for ( let i = 0, l = list.length; i < l; i ++ ) {
                const index = list[i] * 2;
                const offset = i * 2;

                dst_array[offset] = src_array[index];
                dst_array[offset + 1] = src_array[index + 1];

            }

            break;
        case 3:

            for ( let i = 0, l = list.length; i < l; i ++ ) {
                const index = list[i] * 3;
                const offset = i * 3;

                dst_array[offset] = src_array[index];
                dst_array[offset + 1] = src_array[index + 1];
                dst_array[offset + 2] = src_array[index + 2];

            }

            break;
        case 4:

            for ( let i = 0, l = list.length; i < l; i ++ ) {
                const index = list[i] * 4;
                const offset = i * 4;

                dst_array[offset] = src_array[index];
                dst_array[offset+ 1] = src_array[index + 1];
                dst_array[offset + 2] = src_array[index + 2];
                dst_array[offset + 3] = src_array[index + 3];

            }

            break;
    }


    dst.attributes[key] = dst_attribute;


}

dst.boundingSphere = new THREE.Sphere;
dst.computeBoundingSphere();

dst.boundingSphere = new THREE.Box3;
dst.computeBoundingBox();

// Release data
vertices = {};
list = [];

}

return function( precision ) {

prec = Math.pow(10,  precision || 6 );
const geometry = new THREE.BufferGeometry;

indexBufferGeometry(this, geometry);

return geometry;
}
}();
