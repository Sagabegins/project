@extends('layouts.app')

@guest
    <?php
    header('Location: /');
    die();
    ?>
@else
    <?php
    header('Location: /user/'.Auth::id());
    die();
    ?>
@endguest
