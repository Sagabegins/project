@section('topbar')
<div class='top'>
    <div class='navbar navbar-expand-lg navbar-dark bg-dark'>
        <a href='/' class='navbar-brand'>Project Website</a>
        <ul class="navbar-nav" style="margin-left:auto;margin-top:5px">
            <li class="nav-item">
                <a id="Link-Public" class="nav-link" href="/public">Public</a>
            </li>
            <li class="nav-item">
                 <a id="Link-Home" class='nav-link' href="/">Home <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
                <a id="Link-About" class='nav-link' href="/about">About</a>
            </li>
            <li class="nav-item">
                <a id="Link-Login" class='nav-link' href="/login">Login</a>
            </li>
            <li class="nav-item">
                <a id="Link-Register" class="nav-link" href="/register">Register</a>
            </li>
          </ul>
    </div>
</div>
@show

