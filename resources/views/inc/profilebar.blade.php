@section('profilebar')
<div class='top'>
    <div class='navbar navbar-expand-lg navbar-dark bg-dark'>
    <a href="/" class='navbar-brand' style="float:left;font-size:20px;margin:10px;">Project Website</a>
        <ul class="navbar-nav" style='margin-left:auto;margin-top:5px'>
            <li class="nav-item">
                <a id="Link-Public" class="nav-link" href="/public">Public</a>

            </li>
            <li class="nav-item">
                <a id="Link-Profile" class="nav-link" name="{{Auth::id()}}"href="/user/{{Auth::id()}}">Profile</a>
            </li>
            <li class="nav-item">
                <a id="Link-History" class="nav-link" href="/user/{{Auth::id()}}/history/">History</a>
            </li>
            <li class="nav-item">
                    <a id="Link-Logout" class="nav-link" href="{{ route('logout') }}"
                    onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();">
                    Log out
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                    </form>
            </li>
          </ul>
    </div>
</div>
@show

