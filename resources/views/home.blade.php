@extends('layouts.app')

@section('content')
<script>
    var elm = document.getElementById('Link-Home');
    elm.setAttribute("style","color:white")
</script>
<div class='card-deck' style="margin-top:1%;width:100%">
    <div class='text-white card bg-dark' style="height:520px;margin-top:1px;left:1%">
        <div class= 'card-body'>
            <h2 class='card-title'>Welcome to the project website. </h2>
            <p class='card-text'>You can login in to upload your 3d model in stl format for printing.
                If you don't have an account, please register to be able to upload files for getting print ready files.</p>
        </div>
    </div>
    <div class='text-white card bg-dark' style=" height:520px;margin-top:1px;left:1%">
        <div class='card-body'>
            <h2 class='card-title'>Example View</h2>
            <div id='plot'>
                <script src='/js/app.js'></script>
                <script src='/js/display.js'></script>
                <script>display('plot','untitled.stl')</script>
            </div>
        </div>
    </div>
</div>
@endsection

