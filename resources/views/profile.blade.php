@extends('layouts.app')

@guest
<?php
    header('Location: /login');
    die();
?>
@else<?php
$id = Auth::id();
?>
@section('content')
    <script>
        var elm = document.getElementById('Link-Profile');
        elm.setAttribute("style","color:white")
    </script>
    <div class="card-deck bg-secondary" style="margin-top:0.8%;width:100%">
        <div class="text-white card bg-dark"  style="height:561.95px;width:40%;left:1%">
            <div class="card-header">
                <h4 class="card-title">Open to view your file. Upload to save for later.</h4>
            </div>
            <div class="card-body">
                <div id='plot'></div>
                {!!Form::open(["url" => "/user/uploaded/".$id, "method"=> "POST","enctype" => "multipart/form-data"])!!}
                <div class="input-group mb-3" style="width:50%;margin-top:10px">
                <div class="custom-file" style="">
                    @csrf
                    {!!Form::file('upload_file',
                    ["id" => "upload_file",
                    "class"=> "custom-file-input",
                     "accept" => ".stl",
                     "onchange" => "handleChange()",
                     "enctype" => "multipart/form-data",
                     "encoding" =>"multipart/form-data",]
                     )
                     !!}
                    <label class="custom-file-label bg-success" id="file_label">Choose file</label>
                </div>
                <span class="input-group-append" style="">
                    {!!Form::submit('Upload',["class"=> "btn border", "id" => "upload", "style"=> "background-color:#e9ecef"])!!}
                </span>
                </div>
            @if(session('success'))
            <div class="alert alert-success" style="width:40%;height:37.7px;float:right;display:flex;width:100%">
                {{session('success')}}
            </div>
            @endif
            @if(session('error'))
            <div class="alert alert-danger" style="width:40%;height:37.7px;float:right;display:flex;width:100%">
                {{session('error')}}
            </div>
            @endif
            </div>
        </div>
        <div class='text-white card bg-dark' style="height:561.95pxm;left:1%;">
            <div class="card-header">
                <h4 class='card-title'>Optimized 3D Object</h4>
            </div>
            <div class='card-body'>
                <div id='plot-optimized'>
                    <script>
                        display('plot-optimized','./untitled.stl')
                    </script>
                </div>
                <button class="input-group-text" style="margin-top:10px;margin-bottom:0px">Download</button>
            </div>
        </div>

<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('js/display.js') }}"></script>
<script>
function handleUploadClick(e){
    e.preventDefault();
}

function handleChange(){
    const file_label= document.getElementById('file_label');
    const file = document.getElementById('upload_file').files[0];
    file_label.innerHTML= file.name;
    var reader = new FileReader();
    reader.onload=(e)=>{
        result = e.target.result;
        display('plot', result);
        display('plot-optimized', result);
    }
    reader.readAsDataURL(file);
}
</script>

@endsection
@endguest
