@extends('layouts.app')

@section('content')
<script>
    var elm = document.getElementById('Link-Public');
    elm.setAttribute("style","color:white")
</script>
<script src='../../js/app.js'></script>
<script src='../../js/display.js'></script>
    <div class="text-white card bg-dark" style="left:35%;width:30%;margin-top:1%">
        <div class="card-header h1" style="text-align:center">Files Uploaded</div>
        <div class="card-body">
            <table class="table" id="table">
            </table>
        </div>
    </div>
<div class="modal" id="plotModal" style="top:10%">
    <div class="modal-dialog">
        <div class="modal-content bg-secondary">
            <div class="modal-body" style="border:none">
                <div id="plot"></div>
            </div>
            <div class="modal-footer" style="border:none">
                <button class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<script>
var oReq = new XMLHttpRequest(); //New request object
oReq.onload = function() {
    window.v = JSON.parse(oReq.response);
    const table = document.getElementById("table");
    if(v.length>0){
        for(let i=0;i<v.length;i++){
            table.innerHTML = table.innerHTML+"<tr><td class='h2 text-white'>"+v[i].file_name+"</td><td><button id="+i +
                ' class="btn btn-success" style="float:right" data-toggle="modal" data-target="#plotModal" onclick="handleViewClicked(event)">View</button></td></tr>';
        }
    }
}
function handleViewClicked(e){
    display('plot',"../../"+window.v[e.target.id].file_path);
}

oReq.open("get", "/history/view/all",true);
oReq.send();
</script>
@endsection

