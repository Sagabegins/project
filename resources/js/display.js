
var plot = document.getElementById('plot');
var loader = new STLLoader();
function display(){
    loader.load( 'untitled.stl', function ( geometry ) {
        var geo = geometry.toIndexed();
        plt(geo['attributes']['position'],geo['index']['array']);
    });
}

display();
function plt(pos,ind){
    var d= [];
    var e=[];
    var f=[];
    var indi=[];
    var indj=[];
    var indk=[];

    pos= pos['array'];

    for(var i=0;i<pos.length-2;i+=3){
        d.push(pos[i]);
        e.push(pos[i+1]);
        f.push(pos[i+2]);
    }

    for(var i=0;i<ind.length;i+=3){
        indi.push(ind[i]);
        indj.push(ind[i+1]);
        indk.push(ind[i+2]);
    }

    var data=[
    {
      opacity:1.0,
      color:'rgb(173, 216, 230)',
      type: 'mesh3d',
      x: d,
      y: e,
      z: f,
      i: indi,
      j: indj,
      k: indk,
    }
    ];
    var layout = {
        paper_bgcolor: '#010104',
        plot_bgcolor: '#000000'
    };
    Plotly.newPlot('plot', data);
}
